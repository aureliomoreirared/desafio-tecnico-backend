import { AggregateRoot } from '@modules/ddd-hex-ca-module/src'

interface CreateCard {
  title: string
  content: string
  list: string
}

export default class Card extends AggregateRoot<string> {
  id: string
  title: string
  content: string
  list: string

  constructor(){
    super()
  }

  static create(input:CreateCard): Card {
    const entity = new Card() 
    entity.title = input.title
    entity.content = input.content
    entity.list = input.list
     return entity
  }
}