import { IRepo } from '@modules/ddd-hex-ca-module/src'
import Card from '../entities/Card'

export default interface ICardRepo extends IRepo<Card> {
  getAll(): Promise<Card[]>
}
