import { Container } from 'inversify'
import AuthenticationConfig from './authentication/Authentication.config'
import CardConfig from './card/Card.config'

export default class UCConfig {
  static IoC(container: Container): Container {
    container = CardConfig.IoC(container)
    container = AuthenticationConfig.IoC(container)
    return container
  }
}