import TYPES from '@crosscutting/Types'
import { Container } from 'inversify'
import LoginUC from './LoginUC'

export default class AuthenticationConfig {
  static IoC(container: Container): Container {
    container.bind<LoginUC>(TYPES.UC).to(LoginUC).whenTargetNamed('LoginUC')
    return container
  }

}
