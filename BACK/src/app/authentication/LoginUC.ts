import TYPES from '@crosscutting/Types'
import { IUseCase } from '@modules/ddd-hex-ca-module/src'
import { IsString } from 'class-validator'
import { inject, injectable, named } from 'inversify'
import { IAppConfig } from '@crosscutting/AppConfig'
import IJwtAdapter from '@crosscutting/adapters/jwt/IJwtAdapter'
import { v4 as uuid } from 'uuid'

export class LoginReqUC {

  @IsString()
  login: string
  @IsString()
  senha: string
 
}
@injectable()
export default class LoginUC implements IUseCase< LoginReqUC, string | void>{
  public constructor(
    @inject(TYPES.IAppConfig) private readonly appConfig: IAppConfig,
    @inject(TYPES.IJwtAdapter) private readonly jwt: IJwtAdapter  
  ){ }
  
  
  async execute(input: LoginReqUC): Promise<string | void> {
    if(input.login === this.appConfig.LOGIN_USER && input.senha === this.appConfig.LOGIN_PASS) {
      const now = new Date().getTime()
      return this.jwt.generateToken({
        iat: now,
        sub: 'user.id',
        exp: new Date(now + 60 * 60 * 24 * 1000).getTime(),
        jwtid: uuid(),
      })
    }

    return
  }
}
