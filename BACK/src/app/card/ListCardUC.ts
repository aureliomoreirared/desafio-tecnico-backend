import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IUseCase } from '@modules/ddd-hex-ca-module/src'
import { inject, injectable, named } from 'inversify'

@injectable()
export default class ListCardUC implements IUseCase< string, Card[]>{
  public constructor(
  @inject(TYPES.REPO) @named ('ICardRepo')  private readonly cardRepo: ICardRepo,
  ){ }
  
  
  async execute(): Promise<Card[]> {
    return this.cardRepo.getAll()
  }
}
