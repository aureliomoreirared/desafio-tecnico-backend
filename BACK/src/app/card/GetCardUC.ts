import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IUseCase } from '@modules/ddd-hex-ca-module/src'
import { inject, injectable, named } from 'inversify'
import { IValidator, ValidationError } from '@app/validator/IValidator'

@injectable()
export default class GetCardUC implements IUseCase< string, Card | null>{
  public constructor(
  @inject(TYPES.REPO) @named ('ICardRepo')  private readonly cardRepo: ICardRepo,
  ){ }
  
  
  async execute(id: string): Promise<Card | null> {
    return this.cardRepo.findById(id)
  }
}
