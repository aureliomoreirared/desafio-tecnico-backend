import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IUseCase } from '@modules/ddd-hex-ca-module/src'
import { IsString } from 'class-validator'
import { inject, injectable, named } from 'inversify'
import moment from 'moment'

export class UpdateCardReqUC {
  id: string
  @IsString()
  title: string
  @IsString()
  content: string
  @IsString()
  list: string
 
}
@injectable()
export default class UpdateCardUC implements IUseCase< UpdateCardReqUC, Card | null>{
  public constructor(
  @inject(TYPES.REPO) @named ('ICardRepo')  private readonly cardRepo: ICardRepo,
  ){ }
  
  
  async execute(input: UpdateCardReqUC): Promise<Card | null> {
    const card  = await this.cardRepo.findById(input.id)
    if (card === null) return null
    const update: Card = Card.create(input)
    await this.cardRepo.update(update)
    const now = (moment(new Date())).format('DD/MM/YYYY HH:mm:ss')
    console.log(now + " - Card " + card.id + " - " + card.content + " - Atualizado")
    return card
  }
}
