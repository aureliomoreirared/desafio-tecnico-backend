import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IUseCase } from '@modules/ddd-hex-ca-module/src'
import { inject, injectable, named } from 'inversify'
import moment from 'moment'

@injectable()
export default class DeleteCardUC implements IUseCase< string, Card[] | null>{
  public constructor(
  @inject(TYPES.REPO) @named ('ICardRepo')  private readonly cardRepo: ICardRepo,
  ){ }
  
  
  async execute(id: string): Promise<Card[] | null> {
    const card  = await this.cardRepo.findById(id)
    if (card === null) return null
    await this.cardRepo.delete(id)
    const now = (moment(new Date())).format('DD/MM/YYYY HH:mm:ss')
    console.log(now + " - Card " + card.id + " - " + card.content + " - Removido")
    return this.cardRepo.getAll()
  }
}
