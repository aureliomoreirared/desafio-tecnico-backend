import TYPES from '@crosscutting/Types'
import { Container } from 'inversify'
import CreateCardUC from './CreateCardUC'
import DeleteCardUC from './DeleteCardUC'
import GetCardUC from './GetCardUC'
import ListCardUC from './ListCardUC'
import UpdateCardUC from './UpdateCardUC'

export default class CardConfig {
  static IoC(container: Container): Container {
    container.bind<CreateCardUC>(TYPES.UC).to(CreateCardUC).whenTargetNamed('CreateCardUC')
    container.bind<UpdateCardUC>(TYPES.UC).to(UpdateCardUC).whenTargetNamed('UpdateCardUC')
    container.bind<GetCardUC>(TYPES.UC).to(GetCardUC).whenTargetNamed('GetCardUC')
    container.bind<ListCardUC>(TYPES.UC).to(ListCardUC).whenTargetNamed('ListCardUC')
    container.bind<DeleteCardUC>(TYPES.UC).to(DeleteCardUC ).whenTargetNamed('DeleteCardUC')
    return container
  }

}
