import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IUseCase } from '@modules/ddd-hex-ca-module/src'
import { IsString } from 'class-validator'
import { inject, injectable, named } from 'inversify'
import { ValidationError } from '@app/validator/IValidator'

export class CreateCardReqUC {

  @IsString()
  title: string
  @IsString()
  content: string
  @IsString()
  list: string
 
}
@injectable()
export default class CreateCardUC implements IUseCase< CreateCardReqUC, Card | ValidationError[]>{
  public constructor(
  @inject(TYPES.REPO) @named ('ICardRepo')  private readonly cardRepo: ICardRepo,
  ){ }
  
  
  async execute(input: CreateCardReqUC): Promise<Card> {
    const card  = Card.create(input)
    return this.cardRepo.insert(card)
  }
}
