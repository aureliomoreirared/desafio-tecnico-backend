import env from 'env-var'

export interface IAppConfig {
  ENV: string  
  MONGO_URL: string
  JWT_SECRET_KEY: string
  LOGIN_USER:string
  LOGIN_PASS:string
}

const getConfig = (): IAppConfig => {
  return {
    ENV: env.get('NODE_ENV').required().asString(),        
    MONGO_URL: env.get('MONGO_URL').required().asString(),
    JWT_SECRET_KEY: env.get('JWT_SECRET_KEY').required().asString(),
    LOGIN_USER: env.get('LOGIN_USER').required().asString(),
    LOGIN_PASS: env.get('LOGIN_PASS').required().asString(),
  }
}

const config = getConfig()

export default config