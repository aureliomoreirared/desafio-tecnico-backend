import { ConsoleEventBus, IEventBus } from '@modules/ddd-hex-ca-module/src'
import { Container } from 'inversify'
import ILogger from './adapters/logger/ILogger'
import LoggerAdapter from './adapters/logger/LoggerAdapter'
import TYPES from './Types'
import UCConfig from '@app/UCConfig'
import DomainConfig from '@domain/DomainConfig'
import IJwtAdapter from './adapters/jwt/IJwtAdapter'
import JwtAdapter from './adapters/jwt/JwtAdapter'



export default class IoC {
  private container: Container

  constructor(container: Container = new Container({ skipBaseClassChecks: true, autoBindInjectable: true })) {
    this.container = container
  }
  
  public build(): Container {

    // General
    this.container.bind<IEventBus>(TYPES.IEventBus).to(ConsoleEventBus)
    this.container.bind<IJwtAdapter>(TYPES.IJwtAdapter).to(JwtAdapter)


    // Use cases 
    this.container = UCConfig.IoC(this.container)
    
    // Domain Services
    this.container = DomainConfig.IoC(this.container)
    

    // Adapters
    this.container.bind<ILogger>(TYPES.ILogger).to(LoggerAdapter)
    
    return this.container
  }

  
}