
const TYPES = {

  // General
  Container: Symbol.for('Container'),
  IJwtAdapter: Symbol.for('IJwtAdapter'),
  IAppConfig: Symbol.for('IAppConfig'),
  IEventBus: Symbol.for('IEventBus'),

  // Use cases
  UC: Symbol.for('UC'),

  // Domain Services
  SRV: Symbol.for('SRV'),

  // Repositories
  REPO: Symbol.for('REPO'),
  // Adapters
  ILogger: Symbol.for('ILogger'),

  // Validator
  IValidator: Symbol.for('IValidator')
}

export default TYPES