export default class JWTData {
  sub: string
  iat: number
  exp: number
  jwtid: string
}
