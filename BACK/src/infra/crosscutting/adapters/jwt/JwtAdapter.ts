import { IAppConfig } from '@crosscutting/AppConfig'
import TYPES from '@crosscutting/Types'
import { inject, injectable } from 'inversify'
import jsonwebtoken, { decode, verify } from 'jsonwebtoken'

import JWTData from './_model/JWTData'
import IJwtAdapter from './IJwtAdapter'

@injectable()
export default class JwtAdapter implements IJwtAdapter {
  private readonly jwtSecret
  public get JwtSecret(): string {
    return this.jwtSecret
  }

  constructor(@inject(TYPES.IAppConfig) config: IAppConfig) {
    this.jwtSecret = config.JWT_SECRET_KEY
  }

  isExpired(token: string): boolean {
    const data: JWTData = this.decodeJWT(token)
    const now = new Date()
    const exp = new Date(data.exp)
    
    if (now > exp) return false
    
    return true
  }

  generateToken(data: JWTData): string {
    return jsonwebtoken.sign(data, this.jwtSecret)
  }

  getJWT(authorization: string): string {
    const [, token] = authorization.split(' ')
    return token
  }

  verifyToken(token: string): boolean {
    if (verify(token, this.jwtSecret)) return true
    else return false
  }

  decodeJWT(token: string): JWTData {
    return decode(token) as JWTData
  }
}
