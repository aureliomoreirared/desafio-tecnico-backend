import JWTData from './_model/JWTData'

export default interface IJwtAdapter {
  generateToken(data: JWTData): string
  getJWT(authorization: string): string
  verifyToken(token: string): boolean
  decodeJWT(token: string): JWTData
  isExpired(token: string): boolean
}
