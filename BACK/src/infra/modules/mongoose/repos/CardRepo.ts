
import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IEventBus } from '@modules/ddd-hex-ca-module/src'
import { inject, injectable } from 'inversify'
import { model } from 'mongoose'
import CardSchema from '../schemas/CardSchema'

import { MongooseRepo } from './MongooseRepo'

@injectable()
export default class CardRepo extends MongooseRepo<Card> implements ICardRepo {
  constructor(@inject(TYPES.IEventBus) bus: IEventBus) {
    super(model<Card>('Card', CardSchema), bus)
  }

  async getAll(): Promise<Card[]> {
    const result = await this.model.find()
    const mapping: Card[] = []


    result.forEach((element: any, _index, _array) => {
      mapping.push(element.toObject())
    })
    return mapping
  }

}
