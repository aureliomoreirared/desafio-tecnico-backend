
import Card from '@domain/kanbanBoard/entities/Card'
import { Schema } from 'mongoose'

import { CardMapper } from '../mappers/CardMapper'

const CardSchema = new Schema<Card>(
  {
    id: String,
    title: String,
    content: String,
    list: String,
  },
  {
    collection: 'kanban-board-card',
    toObject: {
      transform: (doc, _res) => {
        const mapper = new CardMapper()
        const result = mapper.map(doc)
        result.id = doc._id.toString()
        return result
      },
    },
  },
)

export default CardSchema
