import { mongoMigrateCli } from 'mongo-migrate-ts'

import migrateMongoConfig from './migrateMongoConfig'

mongoMigrateCli(migrateMongoConfig)
