import { IAppConfig } from '@crosscutting/AppConfig'
import TYPES from '@crosscutting/Types'
import ICardRepo from '@domain/kanbanBoard/repo/ICardRepo'
import { IModule } from '@modules/ddd-hex-ca-module/src'
import { Container, inject, injectable } from 'inversify'
import { connect, set } from 'mongoose'
import CardRepo from './repos/CardRepo'

@injectable()
export default class MongooseModule implements IModule {
  private container: Container
  private appConfig: IAppConfig

  constructor(@inject(TYPES.Container) container: Container, @inject(TYPES.IAppConfig) appConfig: IAppConfig) {
    this.container = container
    this.appConfig = appConfig
    this.IoC()
  }

  IoC(): void {
    this.container.bind<ICardRepo>(TYPES.REPO).to(CardRepo).whenTargetNamed('ICardRepo')
  }

  configureServices(): void {
    return
  }

  async start(): Promise<void> {
    set('debug', true)
    await connect(this.appConfig.MONGO_URL)
  }

  stop(): void | Promise<void> {
    throw new Error('Method not implemented.')
  }
}
