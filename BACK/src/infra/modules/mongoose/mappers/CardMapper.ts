import Card from '@domain/kanbanBoard/entities/Card'

import { Mapper } from './Mapper'

export class CardMapper extends Mapper<Card> {
  constructor() {
    super(Card, [
      {
        target: Card,
        properties: {},
      },
    ])
  }
}
