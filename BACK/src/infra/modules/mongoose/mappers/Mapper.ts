
import { AggregateRoot } from '@modules/ddd-hex-ca-module/src'
import { ClassConstructor, plainToClass } from 'class-transformer'
import { Document } from 'mongoose'

export abstract class Mapper<T extends AggregateRoot> {
  private type: ClassConstructor<T>
  private targets: any[]

  constructor(type: ClassConstructor<T>, targets: any[] = []) {
    this.type = type
    this.targets = targets
  }

  map(doc: Document): T {
    const result: T[] = plainToClass(this.type, [doc.toJSON()], {
      excludePrefixes: ['_'],
      targetMaps: this.targets,
    })
    return result[0]
  }
}
