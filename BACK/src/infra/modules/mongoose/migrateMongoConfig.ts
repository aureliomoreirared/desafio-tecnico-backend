// In this file you can configure migrate-mongo
import path from 'path'

const migrateMongoConfig = {
  migrationsDir: path.resolve(__dirname, 'migrations'),
  migrationsCollection: 'migrations',
  uri: process.env.MONGO_URL,
  options: {
    useUnifiedTopology: true,
  },
}

// Return the config as a promise
export default migrateMongoConfig
