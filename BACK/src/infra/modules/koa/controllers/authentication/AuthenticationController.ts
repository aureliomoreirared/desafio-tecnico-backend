import LoginUC, { LoginReqUC } from '@app/authentication/LoginUC'
import TYPES from '@crosscutting/Types'
import { inject, injectable, named } from 'inversify'
import { Body, JsonController, Post, Res, UnauthorizedError } from 'routing-controllers'
import BaseController from '../BaseController'

@injectable()
@JsonController('/login')
export default class AuthenticationController extends BaseController {
  constructor(
    @inject(TYPES.UC) @named ('LoginUC') private readonly loginUC: LoginUC,
  ) {
    super()
  }

  @Post('/')
  async create(@Body() input: LoginReqUC, @Res() response: any): Promise<string | void> {
    const result: any = await this.loginUC.execute(input)
    if(!result) {
      throw new UnauthorizedError('Unauthorized Access')
    }
    return this.response(response, result)
  }
}