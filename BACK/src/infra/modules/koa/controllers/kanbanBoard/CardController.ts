import CreateCardUC, { CreateCardReqUC } from '@app/card/CreateCardUC'
import DeleteCardUC from '@app/card/DeleteCardUC'
import GetCardUC from '@app/card/GetCardUC'
import ListCardUC from '@app/card/ListCardUC'
import UpdateCardUC, { UpdateCardReqUC } from '@app/card/UpdateCardUC'
import TYPES from '@crosscutting/Types'
import Card from '@domain/kanbanBoard/entities/Card'
import { LoggerMiddleware } from '@modules/koa/middlewares/LoggerMiddleware'
import { ValidationError } from 'class-validator'
import { injectable, inject, named } from 'inversify'
import { JsonController, Post, Body, Res, Param, Get, Delete, HttpCode, OnUndefined, Put, Authorized, UseAfter, UseBefore } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import BaseController from '../BaseController'

@injectable()
@JsonController('/kanbanBoard/card')
export default class CardController extends BaseController {
  constructor(
    @inject(TYPES.UC) @named ('CreateCardUC') private readonly createCardUC: CreateCardUC,
    @inject(TYPES.UC) @named ('UpdateCardUC') private readonly updateCardUC: UpdateCardUC,
    @inject(TYPES.UC) @named ('GetCardUC') private readonly getCardUC: GetCardUC,
    @inject(TYPES.UC) @named ('ListCardUC') private readonly listCardUC: ListCardUC,
    @inject(TYPES.UC) @named ('DeleteCardUC') private readonly deleteCardUC: DeleteCardUC,
  ) {
    super()
  }

  @OpenAPI({ security: [{ basicAuth: [] }] })
  @Post('/')
  @Authorized('CARD_CREATE')
  async create(@Body() input: CreateCardReqUC, @Res() response: any): Promise<Card | ValidationError[]> {
    const result: any = await this.createCardUC.execute(input)
    return this.response(response, result)
  }
  
  @OpenAPI({ security: [{ basicAuth: [] }] })
  @Put('/:cardId')
  @OnUndefined(404)
  @Authorized('CARD_UPDATE')
  @UseAfter(LoggerMiddleware)
  async update(@Body() input: UpdateCardReqUC, @Param('cardId') cardId: string, @Res() response: any): Promise<Card | void> {
    input.id = cardId
    const result: any = await this.updateCardUC.execute(input)
    if(result === null) { return }
    return this.response(response, result)
  }
  
  @OpenAPI({ security: [{ basicAuth: [] }] })
  @Get('/:cardId')
  @Authorized('CARD_GET')
  async getById( @Param('cardId') cardId: string, @Res() response: any): Promise<Card | null> {
    const result: any = await this.getCardUC.execute(cardId)
    return this.response(response, result)
  }

  @OpenAPI({ security: [{ basicAuth: [] }] })
  @Get('/')
  @Authorized('CARD_LIST')
  async list(@Res() response: any): Promise<Card[]> {
    const result: any = await this.listCardUC.execute()
    return this.response(response, result)
  }
  
  @OpenAPI({ security: [{ basicAuth: [] }] })
  @Delete('/:cardId')
  @OnUndefined(404)
  @Authorized('CARD_REMOVE')
  async removeById( @Param('cardId') cardId: string, @Res() response: any): Promise<Card[] | void> {
    const result = await this.deleteCardUC.execute(cardId)
    if(result === null)  return 
    return this.response(response, result)
  }
}


