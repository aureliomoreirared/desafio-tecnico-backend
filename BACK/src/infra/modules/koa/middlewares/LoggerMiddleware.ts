import { injectable } from 'inversify'
import { ExpressMiddlewareInterface } from 'routing-controllers'

@injectable()
export class LoggerMiddleware implements ExpressMiddlewareInterface {
  // interface implementation is optional

  use(context: any, next: (err?: any) => Promise<any>): Promise<any> {
    console.log('do something before execution...');
    return next()
      .then(() => {
         if(context.response.status === 200) {
          
         }
      })
  }
}