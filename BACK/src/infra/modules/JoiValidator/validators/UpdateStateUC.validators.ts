import { IValidator, ValidationError } from '@app/validator/IValidator'
import { injectable } from 'inversify'
import Joi from 'joi'

@injectable()
class UpdateStateUCValidator implements IValidator {
  schema: Joi.ObjectSchema<any>
  constructor() {
    this.schema = Joi.object({
      id: Joi.string().required(),
      name: Joi.string().required()
    })
  }
  validate(input: any): ValidationError {
    const resultValidation = this.schema.validate(input, { abortEarly: false })
    const errors = resultValidation.error?.details.map(error => ({
      code: 'INVALID_PARAMS',
      message: error.message
    }))

    return { errors }
  }

}



export default UpdateStateUCValidator
