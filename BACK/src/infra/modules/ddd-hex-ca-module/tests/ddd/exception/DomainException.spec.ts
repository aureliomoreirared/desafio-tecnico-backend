import { DomainException } from '../../../dist/'

describe('DDD', () => {
    describe('DomainException', () => {
        it('should be an instance of DomainException', () => {
            const ex = new DomainException('TEST','error test')
            expect(ex).toBeInstanceOf(DomainException)
        })
        it('should throw a DomainException', () => {
            const throwFunction = () => {
                throw new DomainException('TEST', 'error, test')
            }

            expect(() => throwFunction()).toThrow(DomainException)
        })
    });
});