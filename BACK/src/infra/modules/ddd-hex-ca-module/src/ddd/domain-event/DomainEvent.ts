import { v4 as uuid } from 'uuid'
import { AggregateRoot } from '../aggregate/AggregateRoot'

export abstract class DomainEvent {
  id: string
  type: string
  object: { id: string; type: string }
  data: any
  createdAt: Date

  constructor(entity: AggregateRoot, type: string) {
    this.id = uuid()
    this.type = type
    this.data = entity
    this.createdAt = new Date()
    this.object = {
      id: entity.id,
      type: (entity as {}).constructor.name,
    }
  }
}
