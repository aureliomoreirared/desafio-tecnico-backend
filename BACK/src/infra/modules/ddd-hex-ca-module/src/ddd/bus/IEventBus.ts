import { injectable } from 'inversify'
import { AggregateRoot } from '../aggregate/AggregateRoot'

export interface IEventBus {
  publish(entity: AggregateRoot): void | Promise<void>
}

@injectable()
export class ConsoleEventBus implements IEventBus {
  publish(entity: AggregateRoot<any>): void {
    console.log(entity)
  }
}
