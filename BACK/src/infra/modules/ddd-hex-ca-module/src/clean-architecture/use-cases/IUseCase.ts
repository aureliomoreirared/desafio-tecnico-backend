export interface IUseCase<TReq, TRes> {
    execute(params?: TReq): Promise<TRes> | TRes
}
  