import { APIGatewayEvent, S3Event, SQSEvent } from "aws-lambda"
import { AwsLambdaEventType } from "./types/AwsLambdaEventType"

export class AwsLambdaEventManager{
    
    public static getAwsEventType(event: APIGatewayEvent | S3Event | SQSEvent): AwsLambdaEventType {
        if (this.isS3Event(event as S3Event)) {
          return AwsLambdaEventType.S3
        }
      
        if (this.isApiGatewayAws(event as APIGatewayEvent)) {
          return AwsLambdaEventType.API_GATEWAY_PROXY
        }
      
        if (this.isSQSEvent(event as SQSEvent)) {
          return AwsLambdaEventType.SQS_EVENT
        }
      
        return AwsLambdaEventType.OTHER
      }

      private static isApiGatewayAws(event: APIGatewayEvent): boolean {
        return !!(event.pathParameters && event.headers && event.httpMethod)
      }
      
      private static isS3Event(event: S3Event): boolean {
        return !!(event.Records && event.Records[0].eventSource === 'aws:s3')
      }
      
      private static isSQSEvent (event: SQSEvent): boolean {
        return !!(
          Array.isArray(event.Records) &&
          event.Records.length > 0 &&
          event.Records[0].messageId &&
          event.Records[0].eventSource === 'aws:sqs'
        )
      }

}