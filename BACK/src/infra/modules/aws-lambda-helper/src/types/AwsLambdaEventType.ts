export enum AwsLambdaEventType {
    S3 = 's3',
    API_GATEWAY_PROXY = 'apiGatewayAwsProxy',
    SQS_EVENT = 'sqsEvent',
    CLOUDWATCH_RULE_EVENT = 'cloudwatchRuleEvent',
    FIREHOSE_EVENT = 'firehoseEvent',
    OTHER = 'other',
  }