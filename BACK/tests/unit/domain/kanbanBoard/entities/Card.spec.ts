import Card from '@domain/kanbanBoard/entities/Card'
import faker from 'faker'

describe('Card', () => {
  it('Should create a new Card', () => {
    const expectedCard = {
      title: faker.datatype.string(),
      content: faker.datatype.string(),
      list: faker.datatype.string(),
      events: ([] = [])
    }

    const card = Card.create({
      title: expectedCard.title,
      content: expectedCard.content,
      list: expectedCard.list
    })
    
    expect(card).toEqual(expectedCard)
  })
})
