# Desafio Técnico - Back

Neste desafio eu usei como base um **boilerplate** que fiz para meus projetos **nodejs**, usando uma arquitetura hexagonal e aplicando DDD, também possui uma estrutura de fácil configuração de testes.

# Rodando o projeto

Após clonar o projeto, copie o arquivo .env.example que esta dentro da pasta BACK/ e preencha as variáveis padrões do sistema apontado nele.

NODE_ENV deve ser igual à local

Após configurar o .env basta voltar a pasta raiz e executar o comando 'docker-compose up'
Quando o comando terminar você poderá acessar a api em http://0.0.0.0:5000/ e a sua documentação http://0.0.0.0:5000/swagger

## Debito técnico

No Dockfile uso o --force para instalar as libs, algumas estão conflitando e quando todas são atualizadas o swagger para de funcionar, infelizmente não tive tempo suficiente para resolver esse problema.

Dentro do boilerplate iniciei uma estrutura que favorece a implantação de um controle de eventos, porém, não consegui concluir. Ao implementar o log que foi solicitado, crie o middleware, mas não tive tempo de implementar a captura do card para que o log fosse feito nele, porém, foi adicionado no caso de uso.
